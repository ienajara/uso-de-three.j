      //Crear una escena 
      //THREE
      let escena= new THREE.Scene();
      //Crear la cámara
      let camara= new THREE.PerspectiveCamera(100, window.innerWidth / window.innerHeight, 0.1, 1000);
      //Crear un lienzo (renderer) canvas
      let lienzo= new THREE.WebGLRenderer();
      lienzo.setSize(window.innerWidth, window.innerHeight);
      document.body.appendChild(lienzo.domElement);
      //Crear geometry (Geometria)
      
      let cuboide= new THREE.BoxGeometry();
      let material= new THREE.MeshBasicMaterial({color: 0x70FF00});

      //Crear la malla

      let miCubo= new THREE.Mesh(cuboide, material);
     //Agregar la o las mallas a la escena


      escena.add(miCubo);

      camara.position.z=5;

      let animar= function(){
      
        requestAnimationFrame(animar);
        miCubo.rotation.x=miCubo.rotation.x+0.01;
        miCubo.rotation.y=miCubo.rotation.y+0.02;
        miCubo.rotation.z=miCubo.rotation.z+0.02;
        miCubo.position.x=miCubo.position.x+0.02;
        miCubo.position.y=miCubo.position.y+0.02;
        miCubo.position.z=miCubo.position.z+0.02;

        if(miCubo.position.x < 0.09) 
        
        return miCubo.position.x;
       
    
          lienzo.render(escena,camara);

      }     

      animar();

      //Renderizar: Proceso de dibujar geometrsias en la pantalla

